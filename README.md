# FF Discord

## Introduction

RPG-Discord is a project aiming to simulate a turn-based game system and aiming to achieve its integration and simulation via a bot for the proprietary freeware instant messaging and VoIP application and digital distribution platform Discord, programmed in Python.

In addition to the main goal of a Discord integration, the main constraint is to propose a system very easily modifiable, i.e. containing only the code necessary for its internal functioning: thus, all the elements composing the game(s) based on this system had to be declared "outside" the code in separate files using the same format.

These two aspects have thus largely shaped the way of approaching the programming of the system, described below in more details.

## Core Concept

The game system revolves around the application of effects on entities: each action places one or more effects on its target. On each turn simulated by the system, these effects are "triggered" and perform a series of modifications on their target or caster.

The code managing the game element side of the system is then only a large pyramid scheme leading in fine to the application of effects, serving only as a container for the lower element in the hierarchy while adding a degree of complexity. The hierarchy goes like this:

```mermaid
graph LR
    A(Player) --- B(Weapons)
    B --- C(ActionBar)
    C --- D(Slots)
    D --- E(Abilities)
    E --- F(Spell)
    F --- G(Buffs)
    G --- H(Effects)
    G --- I(Events)
```

From the simplest game element (effect) to the most complex one (Player), the logic goes like this:

- **Effects** as described earlier are just modifier applied each turn to a target.
- **Events** are special trigger used in the game system to reacts to diverse situations (for example, "OnTargetKilled" is the event checked by the system when a Player's target dies).
- **Buffs** define the duration (in terms of turns) of an effect on its target, as well as the event that triggers it.
- **Spells** act as containers for one or multiple buffs. The fact that spells only serve this purpose could seems strange - with not put a list of buffs directly in the abilitiy element? This decision was made to add more depth to the game system, but allowing for example targets to resist spell even if the ability touched.
- **Abilities** are the game element the Players will really interact with: they act as container for one spell and describe it costs, reagents, as well as the spell itself.
- **Slot** are the little squares that the player will frantically smash with his mouse. A slot can contains multiple abilities, activating the former leading to a "switch" to the slot's second ability, which, coupled with a cycling mechanic, allows for combo-based gameplay.
- **ActionBar** do not really need any explanation, as it is only a container of multiple slots. Note howerver as if in the previous graph, ActionBar are linked to weapon, in reality the "weapon" attribute can be anything the Game Design desires them to be.
- **Weapons** should be self-explanatory, but note should be taken that in the context of the system a Weapon is merely described as "an entity with access to an Action Bar". In fact it is possible that this description and concept will move to a new object only dedicated to this purpose in a near future.
- **Player** finally are the entity directly under the control of well, the player of the game.

## Easy Modifications of the Game elements

As stated in the Introduction, the main constraint was to provide a system allowing high customization of its content. As such, from the point of view of a Game Designer, the effects are the only "hardcoded" elements in the system. All the other elements present a static behavior (a spell will always try to apply all of its buffs on its target(s) for example), but all the parameters on which this behavior is based can be modified (to continue with the previous example, the pattern of a spell, its number of buffs, etc).

To allows such system to exists, it needed an universal format of text-representation of the elements of the game. JSON was the one choosen, as it is efficient, easy to read, and already well implemented inside Python thanks to the json library. Thus, each element of the game (minus the Effects) will be described using JSON format, and when starting the game, recovered from files by the system, parsed (with a good error handling and a default solution for each category, avoiding crashes as much as possible) and translated into Python Objects, thus creating a pseudo-database at launch containing all the items of the game.

> Note: At first even Effects were to be modifiable, by allowing the user to create python functions in text files that will be interpreted and stored in memory by the Game System as Effects. it quickly became clear that such a possibility opened the way to serious security issues, as such it was abandoned. It would however be possible to create a language dedicated only to the creation of effects, which would then be parsed and translated into Python Code. This solution could be evaluated in the future.

## Modding

Moreover, this architecture allows for user-friendly modding environment. Indeed, there is 3 problematics that should be addressed from the start when focusing on making a game "moddable" : 
- Allow players to create their own content without needing to dive into the behavioral code of the game
- Minimize conflicts between multiple mods created by different users
- In the specific case of a multiplayer game, make the base files untouchable to prevent users from corrupting the base game

In short, {+ allowing users to create and enjoy as much content as they want with as much ease as possible, while staying within the limits set by the base game. +}
The first problematic is immediatly addressed with the choice of the JSON format to define game entities. JSON is a format really easy to understand even for a uninitiated, and can be easily integrated into fan-made tools to further simplify content creation.

The second and third problematics are resolved by using a specific field in the entities and JSON's files definitions : `origin`. This parameter works like a "container" for entities ID and as an ID for the Mod itself, as each new entity created within a Mod has its own ID but all the entities of this Mod will have the same origin. Thus, each user willing to create a Mod for the game will just have to create a new file, assign an origin to it, and use this origin in all of their new entity.

> Note: The base game also uses the origin parameters : each one of its entity has for origin 0.

In order to ensure that this system works correctly it will also be necessary to set up a tool whose purpose will be to generate and associate a unique origin to each modder - making the origin the modder's ID.
