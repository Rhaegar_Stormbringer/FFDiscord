import class_actionBars as cac

class Weapon():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- WEAPONS --------- #\n")
    for index in Weapon.lst:
      for weapon in Weapon.lst.get(index):
        print("{0} ({1}) [{2}]".format(weapon.name, weapon.ID, weapon.weapon_type))
        print("|\t{0}\n|\tdmg : [{1} - {2}]\n|\tcrit chance : {3}%\n|\tcrit modifier : x{4}\n|\taction bar : {5}".format(weapon.description, weapon.dmg_min, weapon.dmg_max, weapon.crit_chance, weapon.crit_mod, weapon.actionBar.name))
        print("\n------------")
    print("\n")

  # -------------- LOCAL -------------- #
  def __init__(self, ID, origin, name, description, weapon_type, actionBar, dmg_min, dmg_max, dmg_type, crit_chance, crit_mod):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
      self.dmg_min = int(dmg_min)
      self.dmg_max = int(dmg_max)
      self.crit_chance = int(crit_chance)
      self.crit_mod = float(crit_mod)
    except ValueError:
      isValid = False
    self.name = str(name)
    self.description = str(description)
    self.dmg_type = str(dmg_type) #TODO : dmg_type class
    self.owner = None
    self.weapon_type = str(weapon_type) #TODO : weapon_type class
    self.actionBar = None
    n = str(actionBar.get('name', ""))
    try:
      i = int(actionBar.get('id', ""))
    except ValueError:
      i = None
    o = str(actionBar.get('origin', 0))
    if cac.ActionBar.Contains(o):
      if n == "" and i != None:
        self.actionBar = cac.ActionBar.GetActionBarByID(o, i)
      elif n != "" and i == None:
        self.actionBar = cac.ActionBar.GetActionBarByName(o, n)
      else:
        self.actionBar = cac.ActionBar.GetActionBar(o, n, i)
    else:
      self.actionBar = cac.ActionBar.GetDefault()
    
    if (isValid):
      if (origin in Weapon.lst):
        if ([x for x in Weapon.lst.get(origin) if (x.ID == self.ID)] == []):
           Weapon.lst.get(origin).append(self)
      else:
        Weapon.lst[origin] = [self]