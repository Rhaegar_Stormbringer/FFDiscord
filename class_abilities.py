import class_spells as csp

class Ability():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- ABILITIES --------- #\n")
    for index in Ability.lst:
      for ability in Ability.lst.get(index):
        print("{0} ({1})".format(ability.name, ability.ID))
        print("|\t{0} ({1})".format(ability.spell.name, ability.spell.ID))
        print("|\t{0}".format(ability.description))
        print("\n------------")
    print("\n")
  
    '''
  Contains(ID) -> True | False
  Checks if the given ID matches an existing Ability.
  '''
  def Contains(ID):
    if ID in Ability.lst:
      return True
    return False

  '''
  GetDefault() -> Ability.lst.get('0')[0]
  Returns the hardcoded Ability "Nothing", a Ability with a null-effect and used primarily for development and debugging purposes.
  '''
  def GetDefault():
    return Ability.lst.get('0')[0]

  def GetAbilityByID(origin, ID):
    if ID != None:
      return next((x for x in Ability.lst.get(str(origin), []) if x.ID == ID), Ability.GetDefault())
    else:
      return Ability.GetDefault()
  
  def GetAbilityByName(origin, name):
    if name != "":
      return next((x for x in Ability.lst.get(str(origin), []) if x.name.lower() == name.lower()), Ability.GetDefault())
    else:
      return Ability.GetDefault()
  
  def GetAbility(origin, name, ID):
    if name != "" and ID != None:
      return next((x for x in Ability.lst.get(str(origin), []) if (x.ID == ID and x.name.lower() == name.lower())), Ability.GetDefault())
    else:
      return Ability.GetDefault()

  # -------------- LOCAL -------------- #
  def __init__(self, ID, name, origin, description, cost, reagent, cast_time, pattern, spell):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
      self.cast_time = int(cast_time)
      self.cost = int(cost)
    except ValueError:
      isValid = False
    self.name = str(name)
    self.description = str(description)
    self.reagent = reagent
    self.caster = None
    self.target = None
    self.spell = None
    n = str(spell.get('name', ""))
    try:
      i = int(spell.get('id', ""))
    except ValueError:
      i = None
    o = str(spell.get('origin', 0))
    if csp.Spell.Contains(o):
      if n == "" and i != None:
        self.spell = csp.Spell.GetSpellByID(o, i)
      elif n != "" and i == None:
        self.spell = csp.Spell.GetSpellByName(o, n)
      else:
        self.spell = csp.Spell.GetSpell(o, n, i)
    else:
      self.spell = csp.Spell.GetDefault()
    
    
    if (isValid):
      if (origin in Ability.lst):
        if ([x for x in Ability.lst.get(origin) if (x.ID == self.ID)] == []):
           Ability.lst.get(origin).append(self)
      else:
        Ability.lst[origin] = [self]