import class_buffs as cbu

class Spell():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- SPELLS --------- #\n")
    for index in Spell.lst:
      for spell in Spell.lst.get(index):
        print("{0} ({1})".format(spell.name, spell.ID))
        for buff in spell.buffs:
            print("|\t{0} ({1}) [{2}] - [{3} - {4}] ({5}) ".format(buff.name, buff.ID, buff.lifetime, buff.dmg_min, buff.dmg_max, buff.dmg_type))
        print("\n------------")
    print("\n")

  def Contains(ID):
    if ID in Spell.lst:
      return True
    return False

  def GetDefault():
    return Spell.lst.get('0')[0]

  def GetSpellByID(origin, ID):
    if ID != None:
      return next((x for x in Spell.lst.get(str(origin), []) if x.ID == ID), Spell.GetDefault())
    else:
      return Spell.GetDefault()
  
  def GetSpellByName(origin, name):
    if name != "":
      return next((x for x in Spell.lst.get(str(origin), []) if x.name.lower() == name.lower()), Spell.GetDefault())
    else:
      return Spell.GetDefault()
  
  def GetSpell(origin, name, ID):
    if name != "" and ID != None:
      return next((x for x in Spell.lst.get(str(origin), []) if (x.ID == ID and x.name.lower() == name.lower())), Spell.GetDefault())
    else:
      return Spell.GetDefault()
      
  # -------------- LOCAL -------------- #
  def __init__(self, ID, name, origin, dmg_min, dmg_max, dmg_type, debuffs, buffs):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
      self.dmg_min = int(dmg_min)
      self.dmg_max = int(dmg_max)
    except ValueError:
      isValid = False
    self.name = str(name)
    self.dmg_type = str(dmg_type)

    self.debuffs = []
    for d in debuffs:
      print(d)
    
    self.buffs = []
    for b in buffs:
      n = str(b.get('name', ""))
      try:
        i = int(b.get('id', ""))
      except ValueError:
        i = None
      o = str(b.get('origin', 0))
      if cbu.Buff.Contains(o):
        if n == "" and i != None:
          self.AddBuff(cbu.Buff.GetBuffByID(o, i))
        elif n != "" and i == None:
          self.AddBuff(cbu.Buff.GetBuffByName(o, n))
        else:
          self.AddBuff(cbu.Buff.GetBuff(o, n, i))
      else:
        self.AddBuff(cbu.Buff.GetDefault())
    
    if (isValid):
      if (origin in Spell.lst):
        if ([x for x in Spell.lst.get(origin) if (x.ID == self.ID)] == []):
           Spell.lst.get(origin).append(self)
      else:
        Spell.lst[origin] = [self]
  
  def AddBuff(self, buff):
    if (buff not in self.buffs):
      self.buffs.append(buff)
  
  def AddDebuff(self, debuff):
    if (debuff not in self.debuffs):
      self.debuffs.append(debuff)
