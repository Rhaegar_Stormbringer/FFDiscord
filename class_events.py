class Event():

  lst = {}

  def __init__(self, ID, name, origin):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
    except ValueError:
      isValid = False
    self.name = str(name)
  
    if isValid:
      if origin in Event.lst:
        if ([x for x in Event.lst.get(origin) if (x.name == self.name)] == []):
           Event.lst.get(origin).append(self)
      else:
        Event.lst[origin] = [self]