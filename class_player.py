class Player():
  # -------------- GLOBAL -------------- #
  lst = {}

  # -------------- LOCAL -------------- #
  def __init__(self, origin, name):
    isValid = True
    try:
      origin = str(int(origin))
      if (origin in Player.lst):
        self.ID = int(Player.lst.get(origin)[-1].ID + 1)
      else:
        self.ID = 0
    except ValueError:
      isValid = False
    self.name = str(name)
    self.equipement = [] # TODO : retravailler avec un systeme de couple cle / valeur(s) (mais combien de valeur ?)
    self.inventory = [] # TODO : retravailler avec systeme de sacs ?

    if (isValid):
      if (origin in Player.lst):
        if ([x for x in Player.lst.get(origin) if (x.ID == self.ID)] == []):
           Player.lst.get(origin).append(self)
      else:
        Player.lst[origin] = [self]