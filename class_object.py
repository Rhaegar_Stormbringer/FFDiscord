class Object():
  # -------------- GLOBAL -------------- #
  lst = {}

  # -------------- LOCAL -------------- #
  def __init__(self, ID, origin, name, description, type, price):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
      self.price = int(price)
    except ValueError:
      isValid = False
    self.name = str(name)
    self.description = str(description)
    self.type = type # TODO : Faire un lien avec une classe "TYPE_OBJECT"

    # TODO : ajouter Attachment une fois la classe WEAPONS retravaillee
    if (isValid):
      if (origin in Object.lst):
        if ([x for x in Object.lst.get(origin) if (x.ID == self.ID)] == []):
           Object.lst.get(origin).append(self)
      else:
        Object.lst[origin] = [self]