import class_actionBarSlots as cac

class ActionBar():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- ACTION BARS --------- #\n")
    for index in ActionBar.lst:
      for actionBar in ActionBar.lst.get(index):
        print("{0} ({1})".format(actionBar.name, actionBar.ID))
        print("Action Bar composed of {0} slots".format(len(actionBar.slots)))
        for slot in actionBar.slots:
            print("|\t{0} ({1})".format(slot.name, slot.ID))
            for ability in slot.abilities:
              print("\t|\t{0}".format(ability.name))
        print("\n------------")
    print("\n")

  '''
  Contains(ID) -> True | False
  Checks if the given ID matches an existing Action Bar.
  '''
  def Contains(ID):
    if ID in ActionBar.lst:
      return True
    return False

  '''
  GetDefault() -> ActionBar.lst.get('0')[0]
  Returns the hardcoded Action Bar "Nothing", an Action Bar which contains only the slot  "Nothing". This Action Bar is used primarily for development and debugging purposes.
  '''
  def GetDefault():
    return ActionBar.lst.get('0')[0]

  def GetActionBarByID(origin, ID):
    if ID != None:
      return next((x for x in ActionBar.lst.get(str(origin), []) if x.ID == ID), ActionBar.GetDefault())
    else:
      return ActionBar.GetDefault()
  
  def GetActionBarByName(origin, name):
    if name != "":
      return next((x for x in ActionBar.lst.get(str(origin), []) if x.name.lower() == name.lower()), ActionBar.GetDefault())
    else:
      return ActionBar.GetDefault()
  
  def GetActionBar(origin, name, ID):
    if name != "" and ID != None:
      return next((x for x in ActionBar.lst.get(str(origin), []) if (x.ID == ID and x.name.lower() == name.lower())), ActionBar.GetDefault())
    else:
      return ActionBar.GetDefault()
      
  # -------------- LOCAL -------------- #
  def __init__(self, ID, name, origin, actionBarSlots):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
    except ValueError:
      isValid = False
    self.name = str(name)
    self.owner = None
    self.slots = []
    for abs in actionBarSlots:
      n = str(abs.get('name', ""))
      try:
        i = int(abs.get('id', ""))
      except ValueError:
        i = None
      o = str(abs.get('origin', 0))
      if cac.ActionBarSlot.Contains(o):
        if n == "" and i != None:
          self.AddActionBarSlot(cac.ActionBarSlot.GetSlotByID(o, i))
        elif n != "" and i == None:
          self.AddActionBarSlot(cac.ActionBarSlot.GetSlotByName(o, n))
        else:
          self.AddActionBarSlot(cac.ActionBarSlot.GetSlot(o, n, i))
      else:
        self.AddActionBarSlot(cac.ActionBarSlot.GetDefault())

    if (isValid):
      if (origin in ActionBar.lst):
        if ([x for x in ActionBar.lst.get(origin) if (x.ID == self.ID)] == []):
           ActionBar.lst.get(origin).append(self)
      else:
        ActionBar.lst[origin] = [self]
  
  def AddActionBarSlot(self, actionBarSlot):
    if (actionBarSlot not in self.slots):
      self.slots.append(actionBarSlot)