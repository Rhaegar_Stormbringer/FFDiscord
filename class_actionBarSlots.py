import class_abilities as cab

class ActionBarSlot():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- ACTION BAR SLOTS --------- #\n")
    for index in ActionBarSlot.lst:
      for actionBarSlot in ActionBarSlot.lst.get(index):
        print("{0} ({1})".format(actionBarSlot.name, actionBarSlot.ID))
        print("Action Slot composed of {0} abilities".format(len(actionBarSlot.abilities)))
        for ability in actionBarSlot.abilities:
            print("|\t{0} ({1}) [{2} - {3}] uses: {4} | spell: {5} ".format(ability.name, ability.ID, ability.cost, ability.cast_time, ability.reagent, ability.spell.name))
        print("\n------------")
    print("\n")

  '''
  Contains(ID) -> True | False
  Checks if the given ID matches an existing Action Bar Slot.
  '''
  def Contains(ID):
    if ID in ActionBarSlot.lst:
      return True
    return False

  '''
  GetDefault() -> ActionBarSlot.lst.get('0')[0]
  Returns the hardcoded Action Bar Slot "Nothing", an Action Bar Slot which contains only the null-effect ability "Nothing". This Action Bar Slot is used primarily for development and debugging purposes.
  '''
  def GetDefault():
    return ActionBarSlot.lst.get('0')[0]

  def GetSlotByID(origin, ID):
    if ID != None:
      return next((x for x in ActionBarSlot.lst.get(str(origin), []) if x.ID == ID), ActionBarSlot.GetDefault())
    else:
      return ActionBarSlot.GetDefault()
  
  def GetSlotByName(origin, name):
    if name != "":
      return next((x for x in ActionBarSlot.lst.get(str(origin), []) if x.name.lower() == name.lower()), ActionBarSlot.GetDefault())
    else:
      return ActionBarSlot.GetDefault()
  
  def GetSlot(origin, name, ID):
    if name != "" and ID != None:
      return next((x for x in ActionBarSlot.lst.get(str(origin), []) if (x.ID == ID and x.name.lower() == name.lower())), ActionBarSlot.GetDefault())
    else:
      return ActionBarSlot.GetDefault()

  # -------------- LOCAL -------------- #
  def __init__(self, ID, name, origin, abilities):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
    except ValueError:
      isValid = False
    self.name = str(name)
    self.caster = None
    self.target = None
    self.abilities = []
    for a in abilities:
      n = str(a.get('name', ""))
      try:
        i = int(a.get('id', ""))
      except ValueError:
        i = None
      o = str(a.get('origin', 0))
      if cab.Ability.Contains(o):
        if n == "" and i != None:
          self.AddAbility(cab.Ability.GetAbilityByID(o, i))
        elif n != "" and i == None:
          self.AddAbility(cab.Ability.GetAbilityByName(o, n))
        else:
          self.AddAbility(cab.Ability.GetAbility(o, n, i))
      else:
        self.AddAbility(cab.Ability.GetDefault())

    if (isValid):
      if (origin in ActionBarSlot.lst):
        if ([x for x in ActionBarSlot.lst.get(origin) if (x.ID == self.ID)] == []):
           ActionBarSlot.lst.get(origin).append(self)
      else:
        ActionBarSlot.lst[origin] = [self]
  
  
  def AddAbility(self, ability):
    if (ability not in self.abilities):
      self.abilities.append(ability)