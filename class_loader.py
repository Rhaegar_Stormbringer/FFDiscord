import json
import class_events as cev
import class_buffs as cbu
import class_spells as csp
import class_abilities as cab
import class_actionBarSlots as cac
import class_actionBars as cacb
import class_weapons as cwe

class Loader():
  
  def __init__(self, datapath):
    self.datapath = datapath

  def LoadAll(self, datapath=None):
    if (datapath == None):
      datapath = self.datapath
    
    # --- EVENTS --- #
    with open("data/events.json") as f:
      d = json.load(f)['Events']
      
    for e in d:
      cev.Event(ID=e, name=d[e].get('name', "None"), origin=d[e].get('origin', 0))
      
    # --- DEBUFFS --- #
    with open("data/buffs.json") as f:
      d = json.load(f)['Buffs']
      
    for e in d:
      cbu.Buff(ID=e,
                name=d[e].get('name', "None"),
                origin=d[e].get('origin', 0),
                effects=d[e].get('effects', []),
                dmg_min=d[e].get('dmg_min', 0),
                dmg_max=d[e].get('dmg_max', d[e].get('dmg_min', 0)),
                dmg_type=d[e].get('dmg_type', "Physical"),
                lifetime=d[e].get('lifetime', 0)
              )

    cbu.Buff.ShowAll()
    
    # --- SPELLS --- #
    with open("data/spells.json") as f:
      d = json.load(f)['Spells']
      
    for e in d:
      csp.Spell(ID=e,
                  name=d[e].get('name', "Nothing"),
                  origin=d[e].get('origin', 0),
                  debuffs=d[e].get('debuffs', []),
                  buffs=d[e].get('buffs', []),
                  dmg_min=d[e].get('dmg_min', 0),
                  dmg_max=d[e].get('dmg_max', d[e].get('dmg_min', 0)),
                  dmg_type=d[e].get('dmg_type', "Physical"),
                )
      
    csp.Spell.ShowAll()
    
    # --- ABILITIES --- #
    with open("data/abilities.json") as f:
      d = json.load(f)['Abilities']
      
    for e in d:
      cab.Ability(ID=e,
                  name=d[e].get('name', "Nothing"),
                  origin=d[e].get('origin', 0),
                  description=d[e].get('description', ""),
                  cost=d[e].get('cost', 0),
                  reagent=d[e].get('reagent', None),
                  cast_time=d[e].get('cast_time', 0),
                  pattern=d[e].get('pattern', None),
                  spell=d[e].get('spell', {}),
                )
      
    cab.Ability.ShowAll()

    # --- ACTION BAR SLOTS --- #
    with open("data/actionBarSlots.json") as f:
      d = json.load(f)['ActionBarSlots']
    
    for a in d:
      cac.ActionBarSlot(ID=a,
                        name=d[a].get('name', "ABS_Nothing"),
                        origin=d[a].get('origin', 0),
                        abilities=d[a].get('abilities', [])
                  )
    cac.ActionBarSlot.ShowAll()

    # --- ACTION BARS --- #
    with open("data/actionBars.json") as f:
      d = json.load(f)['ActionBars']
    
    for a in d:
      cacb.ActionBar(ID=a,
                    name=d[a].get('name', "AB_Nothing"),
                    origin=d[a].get('origin', 0),
                    actionBarSlots=d[a].get('slots', [])
                  )
    cacb.ActionBar.ShowAll()

    # --- WEAPONS --- #
    print("# ----------------------------------------------------- #")
    with open("data/weapons.json") as f:
      d = json.load(f)['Weapons']
    
    for w in d:
      cwe.Weapon(ID=w,
                 origin=d[w].get('origin', 0),
                 name=d[w].get('name', "Nothing"),
                 description=d[w].get('description', ""),
                 weapon_type=d[w].get('weapon_type', "None"),
                 dmg_min=d[w].get('dmg_min', 0),
                 dmg_max=d[w].get('dmg_max', d[w].get('dmg_min', 0)),
                 dmg_type=d[w].get('dmg_type', "Physical"),
                 crit_chance=d[w].get('crit_chance', 0),
                 crit_mod=d[w].get('crit_mod', 0),
                 actionBar=d[w].get('action_bar', {})
                  )
    cwe.Weapon.ShowAll()