import class_effects as cef

class Buff():
  # -------------- GLOBAL -------------- #
  lst = {}

  def ShowAll():
    print("# --------- BUFFS --------- #\n")
    for index in Buff.lst:
      for buff in Buff.lst.get(index):
        print("{0} ({1})".format(buff.name, buff.ID))
        for endex in buff.effects:
          print("|\t{0}".format(endex))
          for effect in buff.effects.get(endex):
            print("\t|\t{0}".format(effect))
        print("\n------------")
    print("\n")

  '''
  Contains(ID) -> True | False
  Checks if the given ID matches an existing Buff.
  '''
  def Contains(ID):
    if ID in Buff.lst:
      return True
    return False

  '''
  GetDefault() -> Buff.lst.get('0')[0]
  Returns the hardcoded Buff "Nothing", a Buff with a null-effect and used primarily for development and debugging purposes. For more information, see the class_effects.py file.
  '''
  def GetDefault():
    return Buff.lst.get('0')[0]

  def GetBuffByID(origin, ID):
    if ID != None:
      return next((x for x in Buff.lst.get(str(origin), []) if x.ID == ID), Buff.GetDefault())
    else:
      return Buff.GetDefault()
  
  def GetBuffByName(origin, name):
    if name != "":
      return next((x for x in Buff.lst.get(str(origin), []) if x.name.lower() == name.lower()), Buff.GetDefault())
    else:
      return Buff.GetDefault()
  
  def GetBuff(origin, name, ID):
    if name != "" and ID != None:
      return next((x for x in Buff.lst.get(str(origin), []) if (x.ID == ID and x.name.lower() == name.lower())), Buff.GetDefault())
    else:
      return Buff.GetDefault()

  # -------------- LOCAL -------------- #
  def __init__(self, ID, name, origin, effects, dmg_min, dmg_max, dmg_type, lifetime):
    isValid = True
    try:
      self.ID = int(ID)
      origin = str(int(origin))
      self.dmg_min = int(dmg_min)
      self.dmg_max = int(dmg_max)
      if (lifetime != "passive"):
        self.lifetime = int(lifetime)
      else:
        self.lifetime = lifetime
    except ValueError:
      isValid = False
    self.name = str(name)
    
    self.dmg_type = str(dmg_type)
    self.caster = None
    self.target = None
    self.effects = {}
    for e in effects:
      ev = str(e.get('event', None))
      ef = getattr(cef, e.get('effect', "Nothing"))
      if ev != "None" and ef != None:
        if (ev in self.effects):
          self.effects.get(ev).append(ef)
        else:
          self.effects[ev] = [ef]
      else:
        isValid = False
    
    if (isValid):
      if (origin in Buff.lst):
        if ([x for x in Buff.lst.get(origin) if (x.ID == self.ID)] == []):
           Buff.lst.get(origin).append(self)
      else:
        Buff.lst[origin] = [self]